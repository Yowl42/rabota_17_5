#include <iostream>
#include <cmath>

class VectorClass
{
private:
	float x, y, z;
public:
	VectorClass()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	VectorClass(float Start_x, float Start_y, float Start_z)
	{
		VectorClass::x = Start_x;
		VectorClass::y = Start_y;
		VectorClass::z = Start_z;
	}

	// ����� ��� ��������� ����� �������� �������
	void set(float New_x, float New_y, float New_z)
	{
		VectorClass::x = New_x;
		VectorClass::y = New_y;
		VectorClass::z = New_z;
	}

	//������ ��� ��������� ���������
	float GetX()
	{
		return x;
	}
	float GetY()
	{
		return y;
	}
	float GetZ()
	{
		return z;
	}

	//����� ��� ��������� ������ �������
	float VectorModule()
	{
		return sqrt(x * x + y * y + z * z);
	}
};

int main()
{
	VectorClass Vector1;
	std::cout << Vector1.GetX() << ' ' << Vector1.GetY() << ' ' << Vector1.GetZ() << '\n';

	VectorClass Vector2(1, 2, 3);
	std::cout << Vector2.GetX() << ' ' << Vector2.GetY() << ' ' << Vector2.GetZ() << '\n';

	Vector1.set(4, 5, 6);
	std::cout << Vector1.GetX() << ' ' << Vector1.GetY() << ' ' << Vector1.GetZ() << '\n';

	std::cout << Vector1.VectorModule() << ' ' << Vector2.VectorModule();

}